cmd_drivers/sensors/yas537/yas_mag_drv-yas537.o := ccache /home/sleepy/Desktop/gcc-linaro-7.5.0-2019.12-x86_64_aarch64-linux-gnu/bin/aarch64-linux-gnu-gcc -Wp,-MD,drivers/sensors/yas537/.yas_mag_drv-yas537.o.d  -nostdinc -isystem /home/sleepy/Desktop/gcc-linaro-7.5.0-2019.12-x86_64_aarch64-linux-gnu/bin/../lib/gcc/aarch64-linux-gnu/7.5.0/include -I./arch/arm64/include -Iarch/arm64/include/generated  -Iinclude -I./arch/arm64/include/uapi -Iarch/arm64/include/generated/uapi -I./include/uapi -Iinclude/generated/uapi -include ./include/linux/kconfig.h -D__KERNEL__ -mlittle-endian -Wall -Wundef -Wstrict-prototypes -Wno-trigraphs -w -fno-strict-aliasing -fno-common -Wno-format-security -std=gnu89 -DANDROID_VERSION=90000 -DANDROID_MAJOR_VERSION=p -mgeneral-regs-only -fno-pic -mpc-relative-literal-loads -fno-delete-null-pointer-checks -Wno-frame-address -Wno-format-truncation -Wno-format-overflow -Wno-int-in-bool-context -fno-PIE -Os -Wno-maybe-uninitialized -Wno-maybe-uninitialized -Wno-unused-variable -Wno-unused-function --param=allow-store-data-races=0 --param=allow-store-data-races=0 -Wframe-larger-than=2048 -fstack-protector-strong -Wno-unused-but-set-variable -Wno-unused-const-variable -fno-omit-frame-pointer -fno-optimize-sibling-calls -fno-var-tracking-assignments -g -Wdeclaration-after-statement -Wno-pointer-sign -fno-strict-overflow -fno-stack-check -fconserve-stack -Werror=implicit-int -Werror=strict-prototypes -Werror=date-time -DCC_HAVE_ASM_GOTO  -mtune=cortex-a53 -mcpu=cortex-a53 -funsafe-math-optimizations  -D"KBUILD_STR(s)=\#s" -D"KBUILD_BASENAME=KBUILD_STR(yas_mag_drv_yas537)"  -D"KBUILD_MODNAME=KBUILD_STR(yas_mag_drv_yas537)" -c -o drivers/sensors/yas537/.tmp_yas_mag_drv-yas537.o drivers/sensors/yas537/yas_mag_drv-yas537.c

source_drivers/sensors/yas537/yas_mag_drv-yas537.o := drivers/sensors/yas537/yas_mag_drv-yas537.c

deps_drivers/sensors/yas537/yas_mag_drv-yas537.o := \
  drivers/sensors/yas537/yas.h \
  include/linux/types.h \
    $(wildcard include/config/uid16.h) \
    $(wildcard include/config/lbdaf.h) \
    $(wildcard include/config/arch/dma/addr/t/64bit.h) \
    $(wildcard include/config/phys/addr/t/64bit.h) \
    $(wildcard include/config/64bit.h) \
  include/uapi/linux/types.h \
  arch/arm64/include/generated/asm/types.h \
  include/uapi/asm-generic/types.h \
  include/asm-generic/int-ll64.h \
  include/uapi/asm-generic/int-ll64.h \
  arch/arm64/include/uapi/asm/bitsperlong.h \
  include/asm-generic/bitsperlong.h \
  include/uapi/asm-generic/bitsperlong.h \
  include/uapi/linux/posix_types.h \
  include/linux/stddef.h \
  include/uapi/linux/stddef.h \
  include/linux/compiler.h \
    $(wildcard include/config/sparse/rcu/pointer.h) \
    $(wildcard include/config/trace/branch/profiling.h) \
    $(wildcard include/config/profile/all/branches.h) \
    $(wildcard include/config/enable/must/check.h) \
    $(wildcard include/config/enable/warn/deprecated.h) \
    $(wildcard include/config/kprobes.h) \
  include/linux/compiler-gcc.h \
    $(wildcard include/config/arch/supports/optimized/inlining.h) \
    $(wildcard include/config/optimize/inlining.h) \
    $(wildcard include/config/gcov/kernel.h) \
    $(wildcard include/config/arch/use/builtin/bswap.h) \
  arch/arm64/include/uapi/asm/posix_types.h \
  include/uapi/asm-generic/posix_types.h \

drivers/sensors/yas537/yas_mag_drv-yas537.o: $(deps_drivers/sensors/yas537/yas_mag_drv-yas537.o)

$(deps_drivers/sensors/yas537/yas_mag_drv-yas537.o):
