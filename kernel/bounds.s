	.arch armv8-a+crc
	.file	"bounds.c"
// GNU C89 (Linaro GCC 7.5-2019.12) version 7.5.0 (aarch64-linux-gnu)
//	compiled by GNU C version 4.8.4, GMP version 6.1.2, MPFR version 3.1.5, MPC version 1.0.3, isl version none
// GGC heuristics: --param ggc-min-expand=100 --param ggc-min-heapsize=131072
// options passed:  -nostdinc -I ./arch/arm64/include
// -I arch/arm64/include/generated -I include -I ./arch/arm64/include/uapi
// -I arch/arm64/include/generated/uapi -I ./include/uapi
// -I include/generated/uapi -imultiarch aarch64-linux-gnu
// -iprefix /home/sleepy/Desktop/gcc-linaro-7.5.0-2019.12-x86_64_aarch64-linux-gnu/bin/../lib/gcc/aarch64-linux-gnu/7.5.0/
// -isysroot /home/sleepy/Desktop/gcc-linaro-7.5.0-2019.12-x86_64_aarch64-linux-gnu/bin/../aarch64-linux-gnu/libc
// -D __KERNEL__ -D ANDROID_VERSION=90000 -D ANDROID_MAJOR_VERSION=p
// -D CC_HAVE_ASM_GOTO -D KBUILD_STR(s)=#s
// -D KBUILD_BASENAME=KBUILD_STR(bounds)
// -D KBUILD_MODNAME=KBUILD_STR(bounds)
// -isystem /home/sleepy/Desktop/gcc-linaro-7.5.0-2019.12-x86_64_aarch64-linux-gnu/bin/../lib/gcc/aarch64-linux-gnu/7.5.0/include
// -include ./include/linux/kconfig.h kernel/bounds.c -mlittle-endian
// -mgeneral-regs-only -mpc-relative-literal-loads -mtune=cortex-a53
// -mcpu=cortex-a53 -mabi=lp64 -auxbase-strip kernel/bounds.s -g -Os -Wall
// -Wundef -Wstrict-prototypes -Wno-trigraphs -Wno-format-security
// -Wno-frame-address -Wformat-truncation=0 -Wformat-overflow=0
// -Wno-int-in-bool-context -Wno-maybe-uninitialized -Wno-unused-variable
// -Wno-unused-function -Wframe-larger-than=2048
// -Wno-unused-but-set-variable -Wunused-const-variable=0
// -Wdeclaration-after-statement -Wno-pointer-sign -Werror=implicit-int
// -Werror=strict-prototypes -Werror=date-time -w -std=gnu90
// -fno-strict-aliasing -fno-common -fno-delete-null-pointer-checks
// -fno-PIE -fstack-protector-strong -fno-omit-frame-pointer
// -fno-optimize-sibling-calls -fno-var-tracking-assignments
// -fno-strict-overflow -fstack-check=no -fconserve-stack
// -funsafe-math-optimizations -fverbose-asm
// --param allow-store-data-races=0 --param allow-store-data-races=0
// options enabled:  -faggressive-loop-optimizations -falign-functions
// -falign-jumps -falign-labels -falign-loops -fassociative-math
// -fauto-inc-dec -fbranch-count-reg -fcaller-saves
// -fchkp-check-incomplete-type -fchkp-check-read -fchkp-check-write
// -fchkp-instrument-calls -fchkp-narrow-bounds -fchkp-optimize
// -fchkp-store-bounds -fchkp-use-static-bounds
// -fchkp-use-static-const-bounds -fchkp-use-wrappers -fcode-hoisting
// -fcombine-stack-adjustments -fcompare-elim -fcprop-registers
// -fcrossjumping -fcse-follow-jumps -fdefer-pop -fdevirtualize
// -fdevirtualize-speculatively -fdwarf2-cfi-asm -fearly-inlining
// -feliminate-unused-debug-types -fexpensive-optimizations
// -fforward-propagate -ffp-int-builtin-inexact -ffunction-cse -fgcse
// -fgcse-lm -fgnu-runtime -fgnu-unique -fguess-branch-probability
// -fhoist-adjacent-loads -fident -fif-conversion -fif-conversion2
// -findirect-inlining -finline -finline-atomics -finline-functions
// -finline-functions-called-once -finline-small-functions -fipa-bit-cp
// -fipa-cp -fipa-icf -fipa-icf-functions -fipa-icf-variables -fipa-profile
// -fipa-pure-const -fipa-ra -fipa-reference -fipa-sra -fipa-vrp
// -fira-hoist-pressure -fira-share-save-slots -fira-share-spill-slots
// -fisolate-erroneous-paths-dereference -fivopts -fkeep-static-consts
// -fleading-underscore -flifetime-dse -flra-remat -flto-odr-type-merging
// -fmath-errno -fmerge-constants -fmerge-debug-strings
// -fmove-loop-invariants -fomit-frame-pointer -fpartial-inlining
// -fpeephole -fpeephole2 -fplt -fprefetch-loop-arrays -freciprocal-math
// -free -freg-struct-return -freorder-blocks -freorder-functions
// -frerun-cse-after-loop -fsched-critical-path-heuristic
// -fsched-dep-count-heuristic -fsched-group-heuristic -fsched-interblock
// -fsched-last-insn-heuristic -fsched-pressure -fsched-rank-heuristic
// -fsched-spec -fsched-spec-insn-heuristic -fsched-stalled-insns-dep
// -fschedule-fusion -fschedule-insns2 -fsection-anchors
// -fsemantic-interposition -fshow-column -fshrink-wrap
// -fshrink-wrap-separate -fsplit-ivs-in-unroller -fsplit-wide-types
// -fssa-backprop -fssa-phiopt -fstack-protector-strong -fstdarg-opt
// -fstore-merging -fstrict-volatile-bitfields -fsync-libcalls
// -fthread-jumps -ftoplevel-reorder -ftree-bit-ccp -ftree-builtin-call-dce
// -ftree-ccp -ftree-ch -ftree-coalesce-vars -ftree-copy-prop -ftree-cselim
// -ftree-dce -ftree-dominator-opts -ftree-dse -ftree-forwprop -ftree-fre
// -ftree-loop-if-convert -ftree-loop-im -ftree-loop-ivcanon
// -ftree-loop-optimize -ftree-parallelize-loops= -ftree-phiprop -ftree-pre
// -ftree-pta -ftree-reassoc -ftree-scev-cprop -ftree-sink -ftree-slsr
// -ftree-sra -ftree-switch-conversion -ftree-tail-merge -ftree-ter
// -ftree-vrp -funit-at-a-time -funsafe-math-optimizations -fvar-tracking
// -fverbose-asm -fzero-initialized-in-bss -mfix-cortex-a53-835769
// -mfix-cortex-a53-843419 -mgeneral-regs-only -mglibc -mlittle-endian
// -momit-leaf-frame-pointer -mpc-relative-literal-loads

	.text
.Ltext0:
	.cfi_sections	.debug_frame
#APP
	.irp	num,0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30
	.equ	__reg_num_x\num, \num
	.endr
	.equ	__reg_num_xzr, 31

	.macro	mrs_s, rt, sreg
	.inst	0xd5200000|(\sreg)|(__reg_num_\rt)
	.endm

	.macro	msr_s, sreg, rt
	.inst	0xd5000000|(\sreg)|(__reg_num_\rt)
	.endm

#NO_APP
	.align	2
	.global	foo
	.type	foo, %function
foo:
.LFB130:
	.file 1 "kernel/bounds.c"
	.loc 1 16 0
	.cfi_startproc
// kernel/bounds.c:18: 	DEFINE(NR_PAGEFLAGS, __NR_PAGEFLAGS);
	.loc 1 18 0
#APP
// 18 "kernel/bounds.c" 1
	
->NR_PAGEFLAGS 22 __NR_PAGEFLAGS	//
// 0 "" 2
// kernel/bounds.c:19: 	DEFINE(MAX_NR_ZONES, __MAX_NR_ZONES);
	.loc 1 19 0
// 19 "kernel/bounds.c" 1
	
->MAX_NR_ZONES 3 __MAX_NR_ZONES	//
// 0 "" 2
// kernel/bounds.c:21: 	DEFINE(NR_CPUS_BITS, ilog2(CONFIG_NR_CPUS));
	.loc 1 21 0
// 21 "kernel/bounds.c" 1
	
->NR_CPUS_BITS 3 ilog2(CONFIG_NR_CPUS)	//
// 0 "" 2
// kernel/bounds.c:23: 	DEFINE(SPINLOCK_SIZE, sizeof(spinlock_t));
	.loc 1 23 0
// 23 "kernel/bounds.c" 1
	
->SPINLOCK_SIZE 4 sizeof(spinlock_t)	//
// 0 "" 2
// kernel/bounds.c:25: }
	.loc 1 25 0
#NO_APP
	ret
	.cfi_endproc
.LFE130:
	.size	foo, .-foo
.Letext0:
	.file 2 "include/linux/types.h"
	.file 3 "include/linux/init.h"
	.file 4 "./arch/arm64/include/asm/cachetype.h"
	.file 5 "include/linux/printk.h"
	.file 6 "include/linux/kernel.h"
	.file 7 "include/linux/page-flags.h"
	.file 8 "include/linux/mmzone.h"
	.file 9 "include/linux/lockdep.h"
	.section	.debug_info,"",@progbits
.Ldebug_info0:
	.4byte	0x3a1
	.2byte	0x4
	.4byte	.Ldebug_abbrev0
	.byte	0x8
	.uleb128 0x1
	.4byte	.LASF87
	.byte	0x1
	.4byte	.LASF88
	.4byte	.LASF89
	.8byte	.Ltext0
	.8byte	.Letext0-.Ltext0
	.4byte	.Ldebug_line0
	.uleb128 0x2
	.byte	0x1
	.byte	0x6
	.4byte	.LASF0
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.4byte	.LASF1
	.uleb128 0x2
	.byte	0x2
	.byte	0x5
	.4byte	.LASF2
	.uleb128 0x2
	.byte	0x2
	.byte	0x7
	.4byte	.LASF3
	.uleb128 0x3
	.byte	0x4
	.byte	0x5
	.string	"int"
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.4byte	.LASF4
	.uleb128 0x2
	.byte	0x8
	.byte	0x5
	.4byte	.LASF5
	.uleb128 0x2
	.byte	0x8
	.byte	0x7
	.4byte	.LASF6
	.uleb128 0x2
	.byte	0x8
	.byte	0x7
	.4byte	.LASF7
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.4byte	.LASF8
	.uleb128 0x4
	.4byte	0x6c
	.uleb128 0x2
	.byte	0x8
	.byte	0x5
	.4byte	.LASF9
	.uleb128 0x5
	.byte	0x8
	.4byte	0x6c
	.uleb128 0x6
	.4byte	.LASF11
	.byte	0x2
	.byte	0x1d
	.4byte	0x90
	.uleb128 0x2
	.byte	0x1
	.byte	0x2
	.4byte	.LASF10
	.uleb128 0x6
	.4byte	.LASF12
	.byte	0x3
	.byte	0x93
	.4byte	0xa2
	.uleb128 0x5
	.byte	0x8
	.4byte	0xa8
	.uleb128 0x7
	.4byte	0x49
	.uleb128 0x5
	.byte	0x8
	.4byte	0xb3
	.uleb128 0x8
	.uleb128 0x9
	.4byte	0x97
	.4byte	0xbf
	.uleb128 0xa
	.byte	0
	.uleb128 0xb
	.4byte	.LASF13
	.byte	0x3
	.byte	0x96
	.4byte	0xb4
	.uleb128 0xb
	.4byte	.LASF14
	.byte	0x3
	.byte	0x96
	.4byte	0xb4
	.uleb128 0xb
	.4byte	.LASF15
	.byte	0x3
	.byte	0x97
	.4byte	0xb4
	.uleb128 0xb
	.4byte	.LASF16
	.byte	0x3
	.byte	0x97
	.4byte	0xb4
	.uleb128 0x9
	.4byte	0x6c
	.4byte	0xf6
	.uleb128 0xa
	.byte	0
	.uleb128 0xb
	.4byte	.LASF17
	.byte	0x3
	.byte	0x9e
	.4byte	0xeb
	.uleb128 0xb
	.4byte	.LASF18
	.byte	0x3
	.byte	0x9f
	.4byte	0x7f
	.uleb128 0xb
	.4byte	.LASF19
	.byte	0x3
	.byte	0xa0
	.4byte	0x50
	.uleb128 0xb
	.4byte	.LASF20
	.byte	0x3
	.byte	0xac
	.4byte	0xad
	.uleb128 0xb
	.4byte	.LASF21
	.byte	0x3
	.byte	0xae
	.4byte	0x85
	.uleb128 0xb
	.4byte	.LASF22
	.byte	0x4
	.byte	0x28
	.4byte	0x65
	.uleb128 0x9
	.4byte	0x73
	.4byte	0x143
	.uleb128 0xa
	.byte	0
	.uleb128 0x4
	.4byte	0x138
	.uleb128 0xb
	.4byte	.LASF23
	.byte	0x5
	.byte	0xa
	.4byte	0x143
	.uleb128 0xb
	.4byte	.LASF24
	.byte	0x5
	.byte	0xb
	.4byte	0x143
	.uleb128 0x9
	.4byte	0x49
	.4byte	0x169
	.uleb128 0xa
	.byte	0
	.uleb128 0xb
	.4byte	.LASF25
	.byte	0x5
	.byte	0x32
	.4byte	0x15e
	.uleb128 0xb
	.4byte	.LASF26
	.byte	0x5
	.byte	0xa2
	.4byte	0x49
	.uleb128 0xb
	.4byte	.LASF27
	.byte	0x5
	.byte	0xa3
	.4byte	0x49
	.uleb128 0xb
	.4byte	.LASF28
	.byte	0x5
	.byte	0xa4
	.4byte	0x49
	.uleb128 0xc
	.4byte	.LASF30
	.uleb128 0x4
	.4byte	0x195
	.uleb128 0xd
	.4byte	.LASF29
	.byte	0x5
	.2byte	0x1b6
	.4byte	0x19a
	.uleb128 0xc
	.4byte	.LASF31
	.uleb128 0xb
	.4byte	.LASF32
	.byte	0x6
	.byte	0xeb
	.4byte	0x1ab
	.uleb128 0xe
	.4byte	0x78
	.4byte	0x1ca
	.uleb128 0xf
	.4byte	0x49
	.byte	0
	.uleb128 0xb
	.4byte	.LASF33
	.byte	0x6
	.byte	0xec
	.4byte	0x1d5
	.uleb128 0x5
	.byte	0x8
	.4byte	0x1bb
	.uleb128 0xd
	.4byte	.LASF34
	.byte	0x6
	.2byte	0x1a5
	.4byte	0x49
	.uleb128 0xd
	.4byte	.LASF35
	.byte	0x6
	.2byte	0x1a6
	.4byte	0x49
	.uleb128 0xd
	.4byte	.LASF36
	.byte	0x6
	.2byte	0x1a7
	.4byte	0x49
	.uleb128 0xd
	.4byte	.LASF37
	.byte	0x6
	.2byte	0x1a8
	.4byte	0x49
	.uleb128 0xd
	.4byte	.LASF38
	.byte	0x6
	.2byte	0x1a9
	.4byte	0x49
	.uleb128 0xd
	.4byte	.LASF39
	.byte	0x6
	.2byte	0x1aa
	.4byte	0x49
	.uleb128 0xd
	.4byte	.LASF40
	.byte	0x6
	.2byte	0x1bc
	.4byte	0x49
	.uleb128 0xd
	.4byte	.LASF41
	.byte	0x6
	.2byte	0x1be
	.4byte	0x85
	.uleb128 0x10
	.4byte	.LASF50
	.byte	0x7
	.byte	0x4
	.4byte	0x50
	.byte	0x6
	.2byte	0x1c1
	.4byte	0x26c
	.uleb128 0x11
	.4byte	.LASF42
	.byte	0
	.uleb128 0x11
	.4byte	.LASF43
	.byte	0x1
	.uleb128 0x11
	.4byte	.LASF44
	.byte	0x2
	.uleb128 0x11
	.4byte	.LASF45
	.byte	0x3
	.uleb128 0x11
	.4byte	.LASF46
	.byte	0x4
	.byte	0
	.uleb128 0xd
	.4byte	.LASF47
	.byte	0x6
	.2byte	0x1c7
	.4byte	0x23b
	.uleb128 0xd
	.4byte	.LASF48
	.byte	0x6
	.2byte	0x1d9
	.4byte	0x143
	.uleb128 0xd
	.4byte	.LASF49
	.byte	0x6
	.2byte	0x1e4
	.4byte	0x143
	.uleb128 0x12
	.4byte	.LASF51
	.byte	0x7
	.byte	0x4
	.4byte	0x50
	.byte	0x7
	.byte	0x4a
	.4byte	0x34a
	.uleb128 0x11
	.4byte	.LASF52
	.byte	0
	.uleb128 0x11
	.4byte	.LASF53
	.byte	0x1
	.uleb128 0x11
	.4byte	.LASF54
	.byte	0x2
	.uleb128 0x11
	.4byte	.LASF55
	.byte	0x3
	.uleb128 0x11
	.4byte	.LASF56
	.byte	0x4
	.uleb128 0x11
	.4byte	.LASF57
	.byte	0x5
	.uleb128 0x11
	.4byte	.LASF58
	.byte	0x6
	.uleb128 0x11
	.4byte	.LASF59
	.byte	0x7
	.uleb128 0x11
	.4byte	.LASF60
	.byte	0x8
	.uleb128 0x11
	.4byte	.LASF61
	.byte	0x9
	.uleb128 0x11
	.4byte	.LASF62
	.byte	0xa
	.uleb128 0x11
	.4byte	.LASF63
	.byte	0xb
	.uleb128 0x11
	.4byte	.LASF64
	.byte	0xc
	.uleb128 0x11
	.4byte	.LASF65
	.byte	0xd
	.uleb128 0x11
	.4byte	.LASF66
	.byte	0xe
	.uleb128 0x11
	.4byte	.LASF67
	.byte	0xf
	.uleb128 0x11
	.4byte	.LASF68
	.byte	0x10
	.uleb128 0x11
	.4byte	.LASF69
	.byte	0x11
	.uleb128 0x11
	.4byte	.LASF70
	.byte	0x12
	.uleb128 0x11
	.4byte	.LASF71
	.byte	0x13
	.uleb128 0x11
	.4byte	.LASF72
	.byte	0x14
	.uleb128 0x11
	.4byte	.LASF73
	.byte	0x15
	.uleb128 0x11
	.4byte	.LASF74
	.byte	0x16
	.uleb128 0x11
	.4byte	.LASF75
	.byte	0x8
	.uleb128 0x11
	.4byte	.LASF76
	.byte	0xc
	.uleb128 0x11
	.4byte	.LASF77
	.byte	0x8
	.uleb128 0x11
	.4byte	.LASF78
	.byte	0x4
	.uleb128 0x11
	.4byte	.LASF79
	.byte	0xb
	.byte	0
	.uleb128 0x10
	.4byte	.LASF80
	.byte	0x7
	.byte	0x4
	.4byte	0x50
	.byte	0x8
	.2byte	0x119
	.4byte	0x375
	.uleb128 0x11
	.4byte	.LASF81
	.byte	0
	.uleb128 0x11
	.4byte	.LASF82
	.byte	0x1
	.uleb128 0x11
	.4byte	.LASF83
	.byte	0x2
	.uleb128 0x11
	.4byte	.LASF84
	.byte	0x3
	.byte	0
	.uleb128 0xb
	.4byte	.LASF85
	.byte	0x9
	.byte	0x10
	.4byte	0x49
	.uleb128 0xb
	.4byte	.LASF86
	.byte	0x9
	.byte	0x11
	.4byte	0x49
	.uleb128 0x13
	.string	"foo"
	.byte	0x1
	.byte	0xf
	.8byte	.LFB130
	.8byte	.LFE130-.LFB130
	.uleb128 0x1
	.byte	0x9c
	.byte	0
	.section	.debug_abbrev,"",@progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x1b
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x7
	.uleb128 0x10
	.uleb128 0x17
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0x8
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x26
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0x15
	.byte	0
	.uleb128 0x27
	.uleb128 0x19
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x8
	.uleb128 0x15
	.byte	0
	.uleb128 0x27
	.uleb128 0x19
	.byte	0
	.byte	0
	.uleb128 0x9
	.uleb128 0x1
	.byte	0x1
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xa
	.uleb128 0x21
	.byte	0
	.byte	0
	.byte	0
	.uleb128 0xb
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3c
	.uleb128 0x19
	.byte	0
	.byte	0
	.uleb128 0xc
	.uleb128 0x13
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3c
	.uleb128 0x19
	.byte	0
	.byte	0
	.uleb128 0xd
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3c
	.uleb128 0x19
	.byte	0
	.byte	0
	.uleb128 0xe
	.uleb128 0x15
	.byte	0x1
	.uleb128 0x27
	.uleb128 0x19
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xf
	.uleb128 0x5
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x10
	.uleb128 0x4
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x11
	.uleb128 0x28
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x1c
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x12
	.uleb128 0x4
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x13
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0x19
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x7
	.uleb128 0x40
	.uleb128 0x18
	.uleb128 0x2117
	.uleb128 0x19
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_aranges,"",@progbits
	.4byte	0x2c
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x8
	.byte	0
	.2byte	0
	.2byte	0
	.8byte	.Ltext0
	.8byte	.Letext0-.Ltext0
	.8byte	0
	.8byte	0
	.section	.debug_line,"",@progbits
.Ldebug_line0:
	.section	.debug_str,"MS",@progbits,1
.LASF75:
	.string	"PG_checked"
.LASF80:
	.string	"zone_type"
.LASF29:
	.string	"kmsg_fops"
.LASF49:
	.string	"hex_asc_upper"
.LASF23:
	.string	"linux_banner"
.LASF37:
	.string	"panic_on_unrecovered_nmi"
.LASF6:
	.string	"long long unsigned int"
.LASF46:
	.string	"SYSTEM_RESTART"
.LASF33:
	.string	"panic_blink"
.LASF44:
	.string	"SYSTEM_HALT"
.LASF74:
	.string	"__NR_PAGEFLAGS"
.LASF5:
	.string	"long long int"
.LASF0:
	.string	"signed char"
.LASF16:
	.string	"__security_initcall_end"
.LASF18:
	.string	"saved_command_line"
.LASF87:
	.ascii	"GNU C89 7.5.0 -mlittle-endian -mgeneral-regs-only -mpc-relat"
	.ascii	"ive-literal-loads -mtune=cortex-a53 -mcpu=cortex-a53 -mabi=l"
	.ascii	"p64 -g -Os -std=gnu90 -fno-strict-aliasing -fno-common -fno-"
	.ascii	"delete-null-pointer-checks -fno-PIE"
	.string	" -fstack-protector-strong -fno-omit-frame-pointer -fno-optimize-sibling-calls -fno-var-tracking-assignments -fno-strict-overflow -fstack-check=no -fconserve-stack -funsafe-math-optimizations --param allow-store-data-races=0 --param allow-store-data-races=0"
.LASF64:
	.string	"PG_private_2"
.LASF42:
	.string	"SYSTEM_BOOTING"
.LASF51:
	.string	"pageflags"
.LASF88:
	.string	"kernel/bounds.c"
.LASF9:
	.string	"long int"
.LASF61:
	.string	"PG_arch_1"
.LASF67:
	.string	"PG_tail"
.LASF24:
	.string	"linux_proc_banner"
.LASF52:
	.string	"PG_locked"
.LASF57:
	.string	"PG_lru"
.LASF54:
	.string	"PG_referenced"
.LASF12:
	.string	"initcall_t"
.LASF30:
	.string	"file_operations"
.LASF4:
	.string	"unsigned int"
.LASF89:
	.string	"/home/sleepy/Desktop/Helios_7870"
.LASF63:
	.string	"PG_private"
.LASF40:
	.string	"root_mountflags"
.LASF7:
	.string	"long unsigned int"
.LASF28:
	.string	"kptr_restrict"
.LASF31:
	.string	"atomic_notifier_head"
.LASF25:
	.string	"console_printk"
.LASF43:
	.string	"SYSTEM_RUNNING"
.LASF3:
	.string	"short unsigned int"
.LASF11:
	.string	"bool"
.LASF78:
	.string	"PG_savepinned"
.LASF27:
	.string	"dmesg_restrict"
.LASF14:
	.string	"__con_initcall_end"
.LASF85:
	.string	"prove_locking"
.LASF70:
	.string	"PG_reclaim"
.LASF53:
	.string	"PG_error"
.LASF77:
	.string	"PG_pinned"
.LASF39:
	.string	"sysctl_panic_on_stackoverflow"
.LASF19:
	.string	"reset_devices"
.LASF47:
	.string	"system_state"
.LASF65:
	.string	"PG_writeback"
.LASF86:
	.string	"lock_stat"
.LASF73:
	.string	"PG_mlocked"
.LASF35:
	.string	"panic_timeout"
.LASF83:
	.string	"ZONE_MOVABLE"
.LASF15:
	.string	"__security_initcall_start"
.LASF32:
	.string	"panic_notifier_list"
.LASF69:
	.string	"PG_mappedtodisk"
.LASF82:
	.string	"ZONE_NORMAL"
.LASF55:
	.string	"PG_uptodate"
.LASF10:
	.string	"_Bool"
.LASF1:
	.string	"unsigned char"
.LASF50:
	.string	"system_states"
.LASF68:
	.string	"PG_swapcache"
.LASF21:
	.string	"initcall_debug"
.LASF2:
	.string	"short int"
.LASF81:
	.string	"ZONE_DMA"
.LASF59:
	.string	"PG_slab"
.LASF84:
	.string	"__MAX_NR_ZONES"
.LASF56:
	.string	"PG_dirty"
.LASF45:
	.string	"SYSTEM_POWER_OFF"
.LASF34:
	.string	"oops_in_progress"
.LASF8:
	.string	"char"
.LASF66:
	.string	"PG_head"
.LASF79:
	.string	"PG_slob_free"
.LASF36:
	.string	"panic_on_oops"
.LASF17:
	.string	"boot_command_line"
.LASF60:
	.string	"PG_owner_priv_1"
.LASF26:
	.string	"printk_delay_msec"
.LASF41:
	.string	"early_boot_irqs_disabled"
.LASF38:
	.string	"panic_on_io_nmi"
.LASF62:
	.string	"PG_reserved"
.LASF58:
	.string	"PG_active"
.LASF22:
	.string	"__icache_flags"
.LASF48:
	.string	"hex_asc"
.LASF76:
	.string	"PG_fscache"
.LASF13:
	.string	"__con_initcall_start"
.LASF20:
	.string	"late_time_init"
.LASF71:
	.string	"PG_swapbacked"
.LASF72:
	.string	"PG_unevictable"
	.ident	"GCC: (Linaro GCC 7.5-2019.12) 7.5.0"
	.section	.note.GNU-stack,"",@progbits
